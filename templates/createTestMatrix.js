const convert = require('xml-js');
const fs = require('fs');
const path = require('path');
const S = require('string')

const xmlFile = path.resolve(process.argv[2]);
const xml = fs.readFileSync(xmlFile);
const json = convert.xml2js(xml, {});

const getId = str => S(str).between(process.argv[3], process.argv[4]).s

const checkStatus = test => {
  const requirementId = getId(test.attributes.name)
  console.log("requirementId:" + requirementId)
  const isFailed = test.elements && test.elements.length > 0
  const status = { [requirementId]: isFailed ? 'failed' : 'passed' }

  return status
}

const testMatrix = json.elements
  .reduce((acc, curr) => [ ...curr.elements, ...acc]).elements
  .map(suite => suite.elements)
  .reduce((acc, curr) => [ ...curr, ...acc ])
  .map(checkStatus)
  .reduce((acc, curr, {}) => ({ ...curr, ...acc }))

fs.writeFile(
  path.join(path.resolve('./'), 'requirements.json'), 
  JSON.stringify(testMatrix), 
  err => {
    if (err) {
    console.error(err)
    return
  }
})
