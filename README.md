# Convert Junit to Requirements

![CI/CD](https://gitlab.com/pkng-demos-bar/components/convert-junit-to-requirements/badges/main/pipeline.svg)
![Release](https://gitlab.com/pkng-demos-bar/components/convert-junit-to-requirements/-/badges/release.svg)

This CI/CD component provides a translation of Junit xml report to requirements.json for compliance satisfaction of requirements [[1]].  
Additionally, the generated 'requirements.json is also stored as a job artifact.

---

## Example

```yaml
include:
  # include the component located in the current project from the current SHA
  - component: gitlab.com/pkng-demos-bar/components/convert-junit-to-requirements/convert-junit@<version>
    inputs:
      STAGE: "test"
      JUNIT_XML_PATH: "./junit.xml"
```

## Components Inputs

| INPUT | DEFAULT | DESCRIPTION |
| --- | --- | --- |
| STAGE |  | The stage name of the job to convert the junit xml report. |
| JUNIT_XML_PATH | ./junit.xml | Path to junit xml report |
| MATCH_PREFIX | "[REQ-" | Prefix to match test name |
| MATCH_POSTFIX | "]" | Postfix to match test name | 

# References
GitLab Documentation Requirements [[1]]  
Python example [[2]]  
ReactJS example - yarn requirements [[3]]  
Video on requirements management - compliance [[4]]  

[1]:https://docs.gitlab.com/ee/user/project/requirements "GitLab Requirements"
[2]:https://gitlab.com/gitlab-org/requiremeents-mgmt/-/blob/master/verification/parse_coverage.py "Python Example"
[3]:https://gitlab.com/tech-marketing/demos/gitlab-agile-demo/awesome-co/consumer-products/web-app/-/blob/master/scripts/createTestMatrix.js "Junit.xml Example"
[4]:https://www.youtube.com/watch?v=UFegXkBcZqw "Requirements Management - Compliance"